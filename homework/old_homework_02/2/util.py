import numpy as np



def relu(m: np.matrix) -> np.matrix:
    """ Relu function returns 0 for every element of m that is
        smaller than 0 and the value of the element if it is 
        greater than 0.

    Args:
        m (np.matrix): input matrix

    Returns:
        np.matrix: element is 0 if element is less then zero and the element else
    """
    return np.maximum(0, m)

def relu_derivation(m: np.matrix) -> np.matrix:
    """ Return the derivation of the ReLu function for the values of a matrix
        The derivation of Relu is:
        d ReLu      | 0 if x == 0
        ------ :=  <
        d x         | 1 else

    Args:
        m (np.matrix): Matrix of elements

    Returns:
        np.matrix: Derivation in every element of the matrix
    """
    return (m != 0).astype(int)

def mean_sqared_error(prediction: np.matrix, target: np.matrix):
        return 0.5 * np.square(prediction - target)