from typing import Any
from util import relu, relu_derivation
import numpy as np


class Layer:

    def __init__(self, n_units: int, input_units: int) -> None:

        self.n_units        = n_units
        self.input_units    = input_units

        # use combined weights and bias matrix (input_units + 1, n_units)
        #   | w_11 w_12 ... w_1n |      w_in := weigth connect input i with unit n
        #   | w_21 w_22 ... w_2n |      
        #   | ...  ...  ... ...  |
        #   | w_i1 w_i2 ... w_in |
        #   | b_1  b_2  ... b_n  |      b_n  := bias of unit n
        self.weights_and_biases     = np.random.random((input_units + 1, n_units))
        # and set bias to zero
        self.weights_and_biases[-1] = np.zeros(n_units)

        # Input in shape (1, input_units + 1) extended version to easily calculate
        # weigths and bias together
        self.layer_input            : np.matrix = None
        
        # matrix of the inputs calculated with weights and biases shape (1, n_units)
        self.layer_preactivation    : np.matrix    = None

        # the layer preactivation after the activation function shape (1, n_units)
        self.layer_activation       : np.matrix    = None

        # for the previous layer
        self.input_gradient = None
    


    def forward_step(self) ->np.matrix:
        """ Return each unit activation calculated by the input and the weigths
            and put into ReLu activation function.
            
            Also set the layer preactivation to the calculated net input, before
            activation and the layer activation to the output of the activation
            function.

        Returns: 
            np.matrix: Activations of the units
        """

        # calculate preactivation by multiplying extended inputs (i_1,i_,2, ... i_n, 1)
        # by the weight bias matrix
        self.layer_preactivation = self.layer_input * self.weights_and_biases

        # Calculate ReLu activation function for every unit 
        self.layer_activation = relu(self.layer_preactivation)

        return self.layer_activation


    
    def backward_step(self, activation_derivation: np.matrix , learning_rate: float = 0.05):
        
        layer_input_transposed      = self.layer_input.T
        pre_activation_derivation   = relu_derivation(self.layer_preactivation) 

        # compute gradient for weights and biases (I believe it works with biases to because layer iput is there 1)
        gradient        = layer_input_transposed * (np.multiply(pre_activation_derivation, activation_derivation))

        # TODO: not sure if the first transpose or the whole formular is correct propably not
        self.input_gradient  = np.delete(np.multiply(pre_activation_derivation, activation_derivation) * self.weights_and_biases.T,-1, axis=1)

        # update weights and biases
        self.weights_and_biases = self.weights_and_biases - learning_rate * gradient    


        

    def set_input(self, i: np.matrix, expanded: bool = False):
        """Set input if shape is correct

        Args:
            i (np.matrix): A matrix with shape (1, input_units)
        """

        if (i.shape == (1, self.input_units) and not expanded) or (i.shape == (1, self.input_units + 1) and expanded):
            if expanded:
                self.layer_input = i
            else:
                self.layer_input = np.append(i, np.matrix([1]), axis=1)
        else:
            print(f"Layer: Shape of input has to be (1,{self.input_units}), but a matrix with shape {i.shape} is given!")