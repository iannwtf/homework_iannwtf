from util import mean_sqared_error
from layer import Layer
import numpy as np

class MultiLayerPerceptron:
    
    def __init__(self, input_units: float, layer_units: list[int]) -> None:
        """ Create a MLP with the given amount of input units and the amount
            of units per layer
            Todo :args
        """
        self.input_units    = input_units
        self.layers         = self.generate_layers(layer_units)

        self.mlp_input      = None
        self.prediction     = None


    def forward_step(self) -> np.matrix:
        """ Predict the target by forward propagate the activations in the MLP

        Returns:
            np.matrix: Derivation in every element of the matrix
        """

        layer_input = self.mlp_input

        for layer in self.layers:
            
            layer.set_input(layer_input,  expanded = False)
            layer.forward_step()

            layer_input = layer.layer_activation
        self.prediction = layer_input

        return self.prediction
    

    def backpropagation(self, target: np.matrix, learning_rate: float = 0.05):
        if target.shape != self.prediction.shape:
            print(f"Backpropagation not possible: Shape of target has to be {self.prediction.shape},\
                  but a matrix with shape {target.shape} is given!")
        
        loss = mean_sqared_error(self.prediction, target)

        for layer in reversed(self.layers):
            layer.backward_step(loss, learning_rate = learning_rate)
            loss = layer.input_gradient

    def generate_layers(self, layer_units: list[int]) -> list[Layer]:
        """ Generate layers of an amount of input units and a list with
            the amount of units per layer.

        Args:
            layer_units (list[int]): how many units should be generated per layer

        Returns:
            list[Layer]: A list of Layer in the order as they are in the mlp
        """
        layers: list[Layer] = []

        layer_inputs        = self.input_units
        
        for n_units in layer_units:
            
            layers.append(Layer(n_units, layer_inputs))

            layer_inputs = n_units


        return layers
    
    def set_input(self, i: np.matrix):
        """Set input if shape is correct

        Args:
            i (np.matrix): A matrix with shape (1, input_units)
        """

        if i.shape == (1, self.input_units):
            
            self.mlp_input = i
        else:
            print(f"MLP: Shape of input has to be (1,{self.input_units}), but a matrix with shape {i.shape} is given!")