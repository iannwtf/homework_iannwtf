from sklearn.utils import shuffle

# Generators to shuffle and portion the training data
def shuffle_data_set(samples: list, targets: list):
    """Generator that shuffles the given data in identical pattern

    Args:
        samples (list): data samples
        targets (list): data targets

    Yields:
        Generator[tuple[np.array,np.array]]: one instance of sample with corresponding target
    """
    samples, targets = shuffle(samples, targets)

    for sample, target in zip(samples,targets):
        yield sample, target


def shuffle_data_set_batched(samples: list, targets: list, minibatch_size: int):
    """ Generator that shuffles the given data in identical pattern and partitions
        them in minibatches of the given size cutting of any rest if existing.

    Args:
        samples (list): data samples
        targets (list): data targets
        minibatch_size (int): size of the outputted data partitions


    Yields:
        Generator[tuple[np.array,np.array]]: one instance of sample with corresponding target
    """

    shuffle_data_generator = shuffle_data_set(samples, targets)

    for batch in range(int(len(samples) / minibatch_size)):
        output_samples = list()
        output_targets = list()
        for data in range(minibatch_size):
            sample, target = next(shuffle_data_generator)

            output_samples.append(sample)
            output_targets.append(target)

        yield output_samples, output_targets
