import numpy as np

class ActivationFunction:

    def activate(preactivation: np.matrix) -> np.matrix:
        pass

    def backwards(activation: np.matrix, preactivation: np.matrix, loss: np.matrix):

                # 17 x 10 / 17 x 10 = 17x10 *  (17x1  / 17 x 10) = 17x10
        return np.multiply( (activation/preactivation), (loss/activation)) # TODO Maybe elementwise? 



class SigmoidActivationFunction(ActivationFunction):

    def activate(preactivation: np.matrix) -> np.matrix:
        """ Apply the sigmoid activation function to the preactivation matrix:
            s = exp(preactivation)/(1+ exp(preactivation))

        Args:
            preactivation (np.matrix): preactivation matrix to apply the activation function to

        Returns:
            np.matrix: Activation matrix where the function has been applied.
        """
        nominator   = np.exp(preactivation)
        denominator = nominator + 1 

        # return activation matrix
        return nominator/denominator 


class SoftmaxActivationFunction(ActivationFunction):

    def activate(preactivation: np.matrix) -> np.matrix:
        """ Apply the softmax activation function to the matrix
        
        Args:
            preactivation (np.matrix): preactivation matrix to apply the activation function to

        Returns:
            np.matrix: Activation matrix where the function has been applied.
        """
        output   = np.exp(preactivation)

        for row in range(len(preactivation)):
            output[row] = output[row] / np.sum(output[row])

        return output
    


class Loss:

    def calculate(predictions: np.matrix, targets: np.matrix) -> np.matrix:
        pass

    def calculate_backwards(predictions: np.matrix, targets: np.matrix) -> np.matrix:
        pass


class CategorialCrossEntropy(Loss):

    def calculate(predictions: np.matrix, targets: np.matrix) -> np.matrix:
        """ Calculate the categorial cross entropy by comparing predicitions and targets


        Args:
            predictions (np.matrix): Predicted data
            targets (np.matrix): True values

        Returns:
            np.matrix: The Losses
        """
        losses = []
        for prediction, target in zip(predictions,targets):
            loss = -np.sum(targets * np.log(prediction).T )
            losses.append(loss)
        return np.matrix(losses).T 


    def calculate_backwards(predictions: np.matrix, targets: np.matrix) -> np.matrix:
        return predictions - targets