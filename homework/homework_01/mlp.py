import numpy as np
from layer import Layer
from util import CategorialCrossEntropy, SigmoidActivationFunction, SoftmaxActivationFunction
from generators import shuffle_data_set_batched

class MultiLayerPerceptron:
    
    def __init__(self, input_units: int, layer_units: list[int]) -> None:
        """ Create a MLP with the given amount of input units and the amount
            of units per layer

        Args:
            input_units (int): number of inputs from the data (e.g. 64)
            layer_units (list[int]): list that contains one int per layers which specifies the number of neurons
        """
        self.input_units            = input_units
        self.layers: list[Layer]    = self.generate_layers(layer_units)


        self.mlp_input      = None
        self.targets        = None

        self.layer_dic: list[dict] = list()

        for i in range(len(layer_units)):
            self.layer_dic.append({ "activation"        : None,
                                    "preactivation"     : None,
                                    "weight_gradient"   : None})


        # prediction ?
        self.prediction     = None


    def generate_layers(self, layer_units: list[int]) -> list[Layer]:
        """ Generate layers of an amount of input units and a list with
            the amount of units per layer.

        Args:
            layer_units (list[int]): how many units should be generated per layer

        Returns:
            list[Layer]: A list of Layer in the order as they are in the mlp
        """
        # create empty list of layers
        layers: list[Layer] = []

        # n_units: number of neurons per layer
        # layer_inputs: the number of neurons of the previous layer (their outputs used as our inputs)

        # start with the number of inputs (e.g. 64)
        layer_inputs        = self.input_units

        for n_units in layer_units:
            
            layers.append(Layer(SigmoidActivationFunction, n_units, layer_inputs))

            # store this layers number of neurons for the next iteration
            layer_inputs = n_units

        # the final layer needs to use the softmax activation function instead (as per 2.3 in the homework sheet)
        layers[-1].activation_function = SoftmaxActivationFunction


        return layers


    def forward_pass(self, samples: np.matrix) -> np.matrix:
        # set training inputs and targets
        layer_input = samples

        activation = samples

        for layer, current_dictionary in zip(self.layers, self.layer_dic):
            
            # extend 1 for the biases
            layer_input = activation#np.hstack([activation, np.matrix(np.ones(activation.shape[0])).T])
            
            # do forward step and log results
            activation, preactivation =  layer.forward_step(layer_input)
            current_dictionary["activation"]     = activation #np.hstack([activation, np.matrix(np.ones(activation.shape[0])).T])

            current_dictionary["preactivation"]  = preactivation #np.hstack([preactivation, np.matrix(np.ones(preactivation.shape[0])).T])


        return activation

    
    def backward_pass(self, loss: np.matrix):
        input_derivation = loss



        for layer, current_dictionary in zip(reversed(self.layers), reversed(self.layer_dic)):
            layer: Layer


            preactivation_derivation = layer.backward_step(current_dictionary["activation"], current_dictionary["preactivation"], input_derivation)
            weights_gradient, input_derivation = layer.weights_backward(preactivation_derivation, current_dictionary["preactivation"])
            
            current_dictionary["weight_gradient"] = weights_gradient.T


    
    def update_weights_and_biases(self, learning_rate: float = 1):
        
        for layer, dictionary in zip(self.layers, self.layer_dic):
            layer.weights_and_biases = layer.weights_and_biases - learning_rate * dictionary["weight_gradient"]


    def train(self, samples: np.matrix, targets: np.matrix, batch_size: int = 10, epochs = 100):
        print("Start training!")
        # go through epochs
        avg_losses  = list()
        losses      = list()
        for epoch in range(epochs):
            print("Epoch ", epoch)

            # generate samples and targets as batches
            for sample_batch, target_batch in shuffle_data_set_batched(samples, targets, batch_size):
                # predict
                prediction = self.forward_pass(np.matrix(sample_batch))
                # calculate loss from the predictions and the targets
                loss = CategorialCrossEntropy.calculate(prediction, np.matrix(target_batch))

                # backward pass and update weights and biases
                self.backward_pass(loss)
                self.update_weights_and_biases(learning_rate = 0.00000000000005)
                losses.append(loss)

            avg_losses.append(np.average(losses))
            losses = []
        
        return avg_losses