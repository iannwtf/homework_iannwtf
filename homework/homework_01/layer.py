import numpy as np
from util import ActivationFunction, SigmoidActivationFunction, SoftmaxActivationFunction



class Layer:

    def __init__(self, activation_function: ActivationFunction, n_units: int, input_units: int) -> None:
        """Construct a new layer

        Weights are created with random values
        Biases are set to 0

        Args:
            activation_function (ActivationFunction): The activation function to be used
            n_units (int): Number of neurons in this layer
            input_units (int): Number of neurons in the previous layer (used as inputs)
        """
        self.activation_function = activation_function
        self.n_units        = n_units
        self.input_units    = input_units

        # use combined weights and bias matrix (input_units + 1, n_units)
        #   | w_11 w_12 ... w_1n |      w_in := weigth connect input i with unit n
        #   | w_21 w_22 ... w_2n |      
        #   | ...  ...  ... ...  |
        #   | w_i1 w_i2 ... w_in |
        #   | b_1  b_2  ... b_n  |      b_n  := bias of unit n
        self.weights_and_biases     = np.random.random((input_units, n_units)) #np.random.random((input_units + 1, n_units))
        # and set bias to zero
        #self.weights_and_biases[-1] = np.zeros(n_units)
        #self.weights_and_biases[-1] = np.ones(n_units)

        # Input in shape (1, input_units + 1) extended version to easily calculate
        # weigths and bias together
        self.layer_input            : np.matrix = None
        
        # matrix of the inputs calculated with weights and biases shape (1, n_units)
        self.layer_preactivation    : np.matrix    = None

        # the layer preactivation after the activation function shape (1, n_units)
        self.layer_activation       : np.matrix    = None

        # for the previous layer
        self.input_gradient = None
    

    def forward_step(self, inp) -> tuple[np.matrix,np.matrix]:
        """ Return each unit activation calculated by the input and the weigths
            and put into ReLu activation function.
            
            Also set the layer preactivation to the calculated net input, before
            activation and the layer activation to the output of the activation
            function.

        Returns: 
            np.matrix: Activations of the units
        """

        self.layer_input = inp
        self.layer_preactivation = inp * self.weights_and_biases


        # Calculate activation function for every unit and batch
        self.layer_activation = self.activation_function.activate(self.layer_preactivation)

        return self.layer_activation, self.layer_preactivation

    def backward_step(self, activation: np.matrix, preactivation: np.matrix, loss: np.matrix):

        return self.activation_function.backwards(activation, preactivation, loss)
        
        




    def weights_backward(self, pre_activiation_derivation: np.matrix, pre_activation: np.matrix) -> np.matrix:

        weights_gradient = pre_activiation_derivation.T * (pre_activation * (1 / self.weights_and_biases.T))

        input_derivation = pre_activiation_derivation * (pre_activation.T * (1 / self.layer_input))


        return weights_gradient, input_derivation
 
