from cat import Cat

def main():

    # Create two cats
    ambrosia    = Cat(name = "Ambrosia")
    mimi        = Cat(name = "Mimi")

    # let cats greet each other
    ambrosia.greet_cat(cat = mimi)


if __name__ == "__main__":
    main()