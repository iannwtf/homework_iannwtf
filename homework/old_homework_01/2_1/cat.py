# for type hint own class
from __future__ import annotations 



class Cat:
    """ A class to represent a cat with a name that can greet other cats.
    """

    def __init__(self, name: str) -> None:
        """A cat with a Name

        Args:
            name (str): Name of the cat
        """
        self._name = name

    def get_name(self) -> str:
        """ Get cats name

        Returns:
            str: Cat name
        """
        return self._name

    def greet_cat(self, cat: Cat):
        """ Greet the given cat by printing a message

        Args:
            cat (Cat): Cat to greet
        """
        print(f"Hello, I'm {self._name}!\nNice to meet you, {cat.get_name()}!")