def main():
    # get all squares of the numbers from 0 to 100
    sqared_numbers = [x*x for x in range(101)]

    print("\nSquared numbers: ", sqared_numbers)


    # get all squares of the even numbers from 0 to 100
    sqared_numbers_even = [x*x for x in range(101) if (x % 2) == 0]
    
    print("\nEven squared numbers: ", sqared_numbers_even)
    print("\n")



if __name__ == "__main__":
    main()