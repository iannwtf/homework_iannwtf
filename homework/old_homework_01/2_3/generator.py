from typing import Generator


def main():
    # create generator with default size 4
    meow_generator = create_meow_generator()

    # print the output of the generator
    for meow in meow_generator:
        print(f"\n\n{meow}")


def create_meow_generator(size: int = 4) -> Generator[str]:
    """ Create a generator that return "Meow " 2 times more often than
        the last time before starting with 1.

    Args:
        size (int, optional): The size of the generator. Defaults to 4.

    Yields:
        Generator[str]: The generator to create "Meow"
    """
    squares = [x*x for x in range(1, size)]
    
    meow  = "Meow "

    for times in squares:
        yield meow * times 


if __name__ == "__main__":
    main()