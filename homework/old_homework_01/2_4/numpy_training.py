import numpy as np

def main():
    mu      = 0 # mean
    sigma   = 1 # standard deviation

    # create a normal distributed 5x5 numpy array
    normal_distributed = np.random.normal(mu, sigma, (5, 5))

    print("\n\nNormal distributed array:\n", normal_distributed)

    
    # Change where array is greater than 0.009 else set it to 42
    arr_sqr_42 = np.where(normal_distributed > 0.009, normal_distributed * normal_distributed, 42)
    
    print("\n\nChange where array is greater than 0.009 else set it to 42:\n", arr_sqr_42)


    # Get fourth column (is 3 because it start counting by 0)
    fourth_column = arr_sqr_42[3, :]

    print(f"\n\nFourth column:\n{fourth_column}\n")



if __name__ == "__main__":
    main()